/**
 * 一些常用的方法
 */
var Util = {
    /**
     * 设置Map容器的高度
     */
    setMapHeight: function () {
        var mapBoxHeight = $(window).height() - 10;
        $('#mapBox').css({height: mapBoxHeight + 'px'});
    }
}

$(document).ready(function () {
    // 百度地图API功能
    var map = new BMap.Map("map");          // 创建地图实例
    !function () { //初始化地图模块相关代码
        map.enableScrollWheelZoom();                            // 启用滚轮放大缩小 map.enableContinuousZoom();                             // 启用地图惯性拖拽，默认禁用 map.enableInertialDragging();                           // 启用连续缩放效果，默认禁用。 map.addControl(new BMap.NavigationControl());           // 添加平移缩放控件
        map.addControl(new BMap.ScaleControl());                // 添加比例尺控件
        map.addControl(new BMap.OverviewMapControl());          // 添加缩略地图控件
        map.addControl(new BMap.MapTypeControl());              // 添加地图类型控件
        map.centerAndZoom(new BMap.Point(longitude,latitude), zoom); // 初始化地图,设置中心点坐标和地图级别
        map.setCurrentCity("苏州市");                             //由于有3D图，需要设置城市哦
    }();
    // 创建CityList对象，并放在citylist_container节点内
    var myCl = new BMapLib.CityList({container: "citylist_container", map: map});
    // 给城市点击时，添加相关操作
    myCl.addEventListener("cityclick", function (e) {
        // 修改当前城市显示
        $('#curCity').innerHTML = e.name;
        // 点击后隐藏城市列表
        $('#cityList').style.display = "none";
    });
    // 给“更换城市”链接添加点击操作
    $('#curCityText').onclick = function () {
        var cl = $('#cityList');
        if (cl.style.display == "none") {
            cl.style.display = "";
        } else {
            cl.style.display = "none";
        }
    };
    // 给城市列表上的关闭按钮添加点击操作
    $('#popup_close').onclick = function () {
        var cl = $('#cityList');
        if (cl.style.display == "none") {
            cl.style.display = "";
        } else {
            cl.style.display = "none";
        }
    };
    Util.setMapHeight();

    /**
     * 进行检索操作
     * @param 关键词
     * @param 当前页码
     */
    function searchAction(keyword, page) {
        page = page || 0;
        var filter = []; //过滤条件
        var url = "http://api.map.baidu.com/geosearch/v3/nearby?callback=?";
        $.getJSON(url, {
            'q': keyword, //检索关键字
            'page_index': page,  //页码
            'page_size': 10,  //每页数量
            'radius': '10000',  //检索半径
            'location': longitude + ',' + latitude,  //检索的中心点
            'geotable_id': 129543,
            'ak': 'DD01f3faa9bdb51a82b33ad28394a36a'  //用户ak
        }, function (e) {
            renderMap(e, page + 1);
        });
    }

    /**
     * 渲染地图模式
     * @param result
     * @param page
     */
    function renderMap(res, page) {
        var content = res.contents;
        var points = [];
        map.clearOverlays();
        points.length = 0;
        if (content.length == 0) {
            return;
        }
        $.each(content, function (i, item) {
            var point = new BMap.Point(item.location[0], item.location[1]);
    		var marker = new BMap.Marker(point);  // 创建标注
    		var label = new BMap.Label(item.title,{
    			offset: new BMap.Size(0, -18)
    		});
    		marker.setLabel(label);
    		map.addOverlay(marker);              // 将标注添加到地图中
            points.push(point);
            marker.addEventListener('click', showInfo);
            function showInfo() {
                var content = "<img src='" + item.headimgurl + "' style='width:111px;height:83px;float:left;margin-right:5px;'/>" +
                    "<p>名称：" + item.title + "</p>" +
                    "<p>地址：" + item.address + "</p>" ;
                //创建检索信息窗口对象
                var searchInfoWindow = new BMapLib.SearchInfoWindow(map, content, {
                    title: item.title,       //标题
                    width: 290,             //宽度
                    panel: "panel",         //检索结果面板
                    enableAutoPan : true, //自动平移
                    enableSendToPhone: true, //是否显示发送到手机按钮
                    searchTypes: [
                        BMAPLIB_TAB_SEARCH,   //周边检索
                        BMAPLIB_TAB_TO_HERE,  //到这里去
                        BMAPLIB_TAB_FROM_HERE //从这里出发
                    ]
                });
                searchInfoWindow.open(marker);
            };
            map.addOverlay(marker);
        });

        /**
         * 分页
         */
        var pagecount = Math.ceil(res.total / 10);
        if (pagecount > 76) {
            pagecount = 76; //最大页数76页
        }
        function PageClick(pageclickednumber) {
            pageclickednumber = parseInt(pageclickednumber);
            $("#pager").pager({
                pagenumber: pageclickednumber,
                pagecount: pagecount,
                showcount: 3,
                buttonClickCallback: PageClick
            });
            searchAction(keyword, pageclickednumber - 1);
        }
        $("#mapPager").pager({pagenumber: page, pagecount: pagecount, showcount: 3, buttonClickCallback: PageClick});
        map.setViewport(points);
    };
    searchAction('', 0)
});