package com.jfinal.weixin.lbs.model;

/**
 * Created by Jack.Zhou on 12/18/2015.
 */
public class CreateGEOColumn extends BaseGEOBean {

    private String name;
    private String key;//必选，同一个geotable内的名字不能相同
    private int type;//必选，枚举值1:Int64, 2:double, 3:string, 4:在线图片url
    private int max_length;//最大值2048，最小值为1，针对string有效，并且string时必填。此值代表utf8的汉字个数，不是字节个数
    private String default_value;
    private int is_sortfilter_field;//必选 1代表是，0代表否。设置后效果详见 LBS云检索最多只能设置15个,只有int或者double类型可以设置
    private int is_search_field;//必选  1代表支持，0为不支持。只有string可以设置检索字段只能用于字符串类型的列且最大长度不能超过512个字节
    private int is_index_field;//必选    用于存储接口查询:1代表支持，0为不支持    注：is_index_field=1 时才能在根据该列属性值检索时检索到数据
    private int is_unique_field;//可选，用于更新，删除，查询：1代表支持 ，0为不支持
    private String geotable_id;//所属于的geotable_id

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMax_length() {
        return max_length;
    }

    public void setMax_length(int max_length) {
        this.max_length = max_length;
    }

    public String getDefault_value() {
        return default_value;
    }

    public void setDefault_value(String default_value) {
        this.default_value = default_value;
    }

    public int getIs_sortfilter_field() {
        return is_sortfilter_field;
    }

    public void setIs_sortfilter_field(int is_sortfilter_field) {
        this.is_sortfilter_field = is_sortfilter_field;
    }

    public int getIs_search_field() {
        return is_search_field;
    }

    public void setIs_search_field(int is_search_field) {
        this.is_search_field = is_search_field;
    }

    public int getIs_index_field() {
        return is_index_field;
    }

    public void setIs_index_field(int is_index_field) {
        this.is_index_field = is_index_field;
    }

    public int getIs_unique_field() {
        return is_unique_field;
    }

    public void setIs_unique_field(int is_unique_field) {
        this.is_unique_field = is_unique_field;
    }

    public String getGeotable_id() {
        return geotable_id;
    }

    public void setGeotable_id(String geotable_id) {
        this.geotable_id = geotable_id;
    }
}
