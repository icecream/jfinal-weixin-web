package com.jfinal.weixin.lbs.model;

import java.io.Serializable;

/**
 * Created by Jack.Zhou on 12/18/2015.
 */
public class BaseGEOBean implements Serializable {

    private String ak;
    private String sn;

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }
}
