package com.jfinal.weixin.lbs.model;

/**
 * Created by Jack.Zhou on 12/18/2015.
 */
public class CreateGEOTable extends BaseGEOBean {

    private String name;
    private int geotype;
    private int is_published;
    private long timestamp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGeotype() {
        return geotype;
    }

    public void setGeotype(int geotype) {
        this.geotype = geotype;
    }

    public int getIs_published() {
        return is_published;
    }

    public void setIs_published(int is_published) {
        this.is_published = is_published;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
