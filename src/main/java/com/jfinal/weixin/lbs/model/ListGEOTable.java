package com.jfinal.weixin.lbs.model;

/**
 * Created by Jack.Zhou on 12/18/2015.
 */
public class ListGEOTable extends BaseGEOBean {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
