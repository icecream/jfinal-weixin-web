package com.jfinal.weixin.lbs.model;

import java.io.Serializable;

public class InLocationBean implements Serializable {
    private String latitude;
    private String longitude;
    private String address;

    public InLocationBean() {
    }

    /**
     * @param latitude  Latitude
     * @param longitude Longitude
     */
    public InLocationBean(String latitude, String longitude, String address) {
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setAddress(address);
    }

    /**
     * @return latitude Latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude Latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return longitude Latitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude Latitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object other) {
        return this.getLatitude().equals(((InLocationBean) other).getLatitude()) && this.getLongitude().equals(((InLocationBean) other).getLongitude());
    }
}




