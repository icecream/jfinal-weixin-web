package com.jfinal.weixin.lbs.model;

/**
 * Created by Jack.Zhou on 12/18/2015.
 */
public class CreateGEOData extends BaseGEOBean {

    private String title;//poi名称
    private String address;//地址
    private String tags;//tags
    private double latitude;//用户上传的纬度x
    private double longitude;//用户上传的经度y
    private int coord_type;//用户上传的坐标的类型 必选    1：GPS经纬度坐标    2：国测局加密经纬度坐标    3：百度加密经纬度坐标    4：百度加密墨卡托坐标
    private String geotable_id;//记录关联的geotable的标识

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getCoord_type() {
        return coord_type;
    }

    public void setCoord_type(int coord_type) {
        this.coord_type = coord_type;
    }

    public String getGeotable_id() {
        return geotable_id;
    }

    public void setGeotable_id(String geotable_id) {
        this.geotable_id = geotable_id;
    }
}
