package com.jfinal.weixin.lbs;

import com.jfinal.aop.Before;
import com.jfinal.weixin.lbs.cache.J2CacheService;
import com.jfinal.weixin.lbs.model.InLocationBean;
import com.jfinal.weixin.sdk.api.ApiResult;

/**
 * Created by jack on 2015/12/26.
 */
public class WeixinLBSController extends WeixinApiController {
    @Before(WeixinUserInfoInterceptor.class)
    public void findNearByUser() {
        ApiResult user = getSessionAttr("userInfo");
        Object location = null;
        if (user != null && user.getStr("openid") != null) {
            location = J2CacheService.get(user.getStr("openid"));
        }
        String latitude = "31.303077";
        String longitude = "120.673515";
        if (location != null) {
            latitude = ((InLocationBean) location).getLatitude();
            longitude = ((InLocationBean) location).getLongitude();
        }
        setAttr("latitude", latitude);
        setAttr("longitude", longitude);
        setAttr("zoom", 14);
        render("/lbs/findNearByUser.html");
    }

    public void index() {
        redirect("/findNearByUser");
    }

}
