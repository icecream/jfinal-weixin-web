package com.jfinal.weixin.lbs;

import com.jfinal.aop.Before;
import com.jfinal.weixin.lbs.cache.J2CacheService;
import com.jfinal.weixin.lbs.model.InLocationBean;
import com.jfinal.weixin.sdk.api.ApiResult;

/**
 * Created by jack on 2015/12/26.
 */
public class PanoController extends WeixinApiController {
    public void index() {
        redirect("/pano/tour.html");
    }

}
