package com.jfinal.weixin.lbs.util;

import org.beetl.core.Context;
import org.beetl.core.Function;

/**
 * JS 参数自动获取并处理默认值
 *
 * @author Jieven
 * @date 2014-5-23
 */
public class JsFormatFun implements Function {
    @Override
    public Object call(Object[] paras, Context ctx) {
        if (paras.length != 1) {
            throw new RuntimeException("参数错误，期望Object");
        }
        Object obj = paras[0];
        if (BaseTypeUtil.isEmpty(obj)) {
            return "undefined";
        }
        if (BaseTypeUtil.isNum(obj)) {
            return obj.toString();
        }
        return BaseTypeUtil.format(obj);
    }
}